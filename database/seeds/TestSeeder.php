<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Test::class, 1)
            ->create()
            ->each(function ($t) {
                $t->results()->saveMany(factory(\App\TestResult::class, 3)->make());
                $t->questions()->saveMany(
                    factory(\App\TestQuestion::class, 10)->create(['test_id' => $t->id])
                        ->each(function ($q) {
                            $q->answers()->saveMany(factory(\App\TestAnswer::class, 4)->make(['question_id' => $q->id]));
                        })
                );
            });
    }
}
