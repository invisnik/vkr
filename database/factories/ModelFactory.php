<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;
    static $is_admin;
    static $is_moderator;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'is_admin' => $is_admin ?: $is_admin = false,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Test::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'desc' => $faker->text,
    ];
});

$factory->define(App\TestQuestion::class, function (Faker $faker) {
    return [
        'text' => $faker->sentence
    ];
});

$factory->define(App\TestAnswer::class, function (Faker $faker) {
    return [
        'text' => $faker->word,
        'points' => random_int(-5, 5)
    ];
});

$factory->define(App\TestResult::class, function (Faker $faker) {
    static $test_id;
    return [
        'text' => $faker->sentence,
        'points_from' => random_int(-5, 0),
        'points_to' => random_int(0, 5)
    ];
});
