<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test user table.
     *
     * @return void
     */
    public function testUserTable()
    {
        $user = factory(\App\User::class)->create();

        $this->assertDatabaseHas('users', [
            'email' => $user->email
        ]);
    }

    /**
     * Test "test" table
     *
     * @return void
     */
    public function testTestTable()
    {
        factory(\App\Test::class, 1)
            ->create()
            ->each(function ($t) {
                $t->results()->saveMany(factory(\App\TestResult::class, 3)->make());
                $t->questions()->saveMany(
                    factory(\App\TestQuestion::class, 10)->create(['test_id' => $t->id])
                        ->each(function ($q) {
                            $q->answers()->saveMany(factory(\App\TestAnswer::class, 4)
                            ->make(['question_id' => $q->id]));
                        })
                );
            });

        $test = \App\Test::with(['results', 'questions.answers'])->first();

        $this->assertDatabaseHas('tests', [
            'name' => $test->name
        ]);

        $this->assertDatabaseHas('test_questions', $test->questions[0]->first()->toArray());
        $this->assertDatabaseHas('test_question_answers', $test->questions[0]->answers->first()->toArray());
        $this->assertDatabaseHas('test_results', $test->results[0]->first()->toArray());
    }
}
