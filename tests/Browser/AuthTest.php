<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * Init user
     *
     * @return void
     */
    public function getUser()
    {
        if ($this->user) {
            return;
        }

        $this->user = factory(\App\User::class)->create([
            'email' => 'john@example.com',
            'password' => bcrypt('secret')
        ]);
    }

    /**
     * Тест неуспешной авторизации пользователя
     *
     * @return void
     */
    public function testAUserReceivesErrorsForWrongLoginCredentials()
    {
        $this->getUser();

        $this->browse(function (Browser $browser) {
            $browser->visit(route('login'))
                ->type('#login_email', $this->user->email)
                ->type('#login_password', 'wrongPassword')
                ->click('#login_button')
                ->pause(5000)
                ->assertVisible('.form-control-feedback');
        });
    }

    /**
     * Тест успешной авторизации пользователя
     *
     * @return void
     */
    public function testUserCanSuccessfullyLogIn()
    {
        $this->getUser();

        $this->browse(function (Browser $browser) {
            $browser->visit(route('login'))
                ->type('#login_email', $this->user->email)
                ->type('#login_password', 'secret')
                ->click('#login_button')
                ->pause(5000)
                ->assertRouteIs('index');
        });
    }

    /**
     * Тест регистрации пользователя
     *
     * @return void
     */
    public function testUserRegister()
    {
        $this->user = factory(\App\User::class)->make();

        $this->browse(function (Browser $browser) {
            $browser->visit(route('login'))
                ->click('#m_login_signup')
                ->assertSee('Зарегистрироваться')
                ->type('#register_fullname', $this->user->first_name . ' ' . $this->user->last_name)
                ->type('#register_email', $this->user->email)
                ->type('#register_password', $this->user->password)
                ->type('#register_cpassword', $this->user->password)
                ->click('#register_button')
                ->pause(5000)
                ->assertRouteIs('index');
        });
    }
}
