import { Model as BaseModel } from 'vue-api-query';
let axios = require('axios');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
	axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
	console.error(
		'CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token'
	);
}

BaseModel.$http = axios;

export default class Model extends BaseModel {
	// define a base url for a REST API
	baseURL() {
		return 'http://vkr.test/api';
	}

	// implement a default request method
	request(config) {
		return this.$http.request(config);
	}
}
