window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
	window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
	console.error(
		'CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token'
	);
}

import Vue from 'vue';
(global || window).Vue = Vue;

import VueRouter from 'vue-router';
import VueProgressBar from 'vue-progressbar';

Vue.use(VueRouter);
Vue.use(VueProgressBar, { color: '#26C281', fail: '#E7505A' });
/**
 * Router initialization
 */
import Router from './Router';
(global || window).router = new Router();

if (document.location.pathname != '/login') {
	router.init(Vue, VueRouter);
}

swal.setDefaults({
	confirmButtonClass: 'btn m-btn--wide btn-outline-success btn-sm',
	cancelButtonClass: 'btn m-btn--wide btn-secondary btn-sm',
	cancelButtonText: 'Отмена'
});
