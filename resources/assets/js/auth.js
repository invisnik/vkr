const authApp = new Vue({
	el: '#authApp',
	data: {
		loginData: {
			email: '',
			password: '',
			remember: false
		},
		registerData: {
			fullname: '',
			email: '',
			password: '',
			password_confirmation: ''
		},
		forgetData: {
			email: ''
		},

		errors: {
			loginErrors: {},
			registerErrors: {},
			forgetErrors: {}
		}
	},

	methods: {
		initFormSwitcher() {
			var SnippetLogin = (function() {
				var login = $('#m_login');

				//== Private Functions

				var displaySignUpForm = function() {
					login.removeClass('m-login--forget-password');
					login.removeClass('m-login--signin');

					login.addClass('m-login--signup');
					mUtil.animateClass(
						login.find('.m-login__signup')[0],
						'flipInX animated'
					);
				};

				var displaySignInForm = function() {
					login.removeClass('m-login--forget-password');
					login.removeClass('m-login--signup');

					login.addClass('m-login--signin');
					mUtil.animateClass(
						login.find('.m-login__signin')[0],
						'flipInX animated'
					);
				};

				var handleFormSwitch = function() {
					$('#m_login_forget_password_cancel').click(function(e) {
						e.preventDefault();
						displaySignInForm();
					});

					$('#m_login_signup').click(function(e) {
						e.preventDefault();
						displaySignUpForm();
					});

					$('#m_login_signup_cancel').click(function(e) {
						e.preventDefault();
						displaySignInForm();
					});
				};

				//== Public Functions
				return {
					// public functions
					init: function() {
						handleFormSwitch();
					}
				};
			})().init();
		},
		login() {
			axios
				.post('/auth/login', this.loginData)
				.then(response => {
					console.log(response.data);
					if (response.data.redirect) {
						window.location.replace(response.data.redirect);
					}
				})
				.catch(error => {
					this.errors.loginErrors = error.response.data.errors;
				});
		},
		register() {
			axios
				.post('/auth/register', this.registerData)
				.then(response => {
					if (response.data.redirect) {
						window.location.replace(response.data.redirect);
					}
				})
				.catch(error => {
					this.errors.registerErrors = error.response.data.errors;
				});
		}
	},

	mounted() {
		this.initFormSwitcher();
	}
});
