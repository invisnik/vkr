import Tests from './pages/Tests.vue'
import Testing from './pages/Testing.vue'
import Dashboard from './pages/Dashboard.vue'
import Profile from './pages/Profile.vue'

import AdminTests from './pages/admin/Tests.vue'
import AdminTestsCreate from './pages/admin/Tests/Create.vue'
import AdminTestsEdit from './pages/admin/Tests/Edit.vue'
import AdminUsers from './pages/admin/Users.vue'
import AdminUsersEdit from './pages/admin/Users/Edit.vue'
import AdminUserGroups from './pages/admin/Users/Groups.vue';
import AdminUserGroupsStats from './pages/admin/Users/Stats.vue';

export default class Router {
    /**
     * Application constructor.
     */
    constructor() {
        this.titleOffset = ' | OTS';

        /**
         * Array of pages
         * @type {[*]}
         */
        this.pages = [
            Tests, Testing,
            Dashboard,
            Profile,
            AdminTests, AdminTestsCreate, AdminTestsEdit,
            AdminUsers, AdminUsersEdit, AdminUserGroups, AdminUserGroupsStats
        ];

        this.initEvents();

        /**
         * @type {{progress: {func: [*]}}}
         */
        this.progressBarMeta = {
            progress: {
                func: [
                    {call: 'color', modifier: 'temp', argument: '#26C281'},
                    {call: 'fail', modifier: 'temp', argument: '#E7505A'},
                    {call: 'location', modifier: 'temp', argument: 'top'},
                    {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.2s', termination: 800}}
                ]
            }
        };

        /**
         * Array of routes
         * @type {Array}
         */
        this.routes = [
            {path: '/', name: 'tests', component: Tests},
            {path: '/test/:id', name: 'test', component: Testing},
            {path: '/dashboard', name: 'dashboard', component: Dashboard},
            {path: '/profile', name: 'profile', component: Profile},
            {path: '/admin/tests', name: 'admin.tests', component: AdminTests},
            {path: '/admin/tests/create', name: 'admin.tests.create', component: AdminTestsCreate},
            {path: '/admin/tests/:id/edit', name: 'admin.tests.edit', component: AdminTestsEdit},
            {path: '/admin/users', name: 'admin.users', component: AdminUsers},
            {path: '/admin/users/:id/edit', name: 'admin.users.edit', component: AdminUsersEdit},
            {path: '/admin/users/groups', name: 'admin.users.groups', component: AdminUserGroups},
            {path: '/admin/users/groups/:id/stats', name: 'admin.users.groups.stats', component: AdminUserGroupsStats},
            { path: '/admin/users/groups/:id/stats/:test_id', name: 'admin.users.groups.test.stats', component: AdminUserGroupsStats},
        ];

        this.routes.map((route) => {
            route.meta = this.progressBarMeta;
            return route;
        });
    }

    /**
     * @param {Vue}
     * @param {VueRouter}
     */
    init(Vue, VueRouter) {
        this.router = new VueRouter({
            mode: 'history',
            base: '/',
            linkExactActiveClass: 'm-menu__item--active',
            routes: this.routes
        })

        this.app = new Vue({
            router: this.router
        }).$mount('#app')
    }

    /**
     * Init events for page
     */
    initEvents() {
        let self = this;
        this.pages.forEach((Page) => {
            Page['created'] = function () {
                //set page title
                document.title = this.title + self.titleOffset;

                if(typeof this.fetchData == 'function') {
                    this.fetchData();
                }

                this.$Progress.start()
                this.$router.beforeEach((to, from, next) => {
                    if (to.meta.progress !== undefined) {
                        let meta = to.meta.progress
                        this.$Progress.parseMeta(meta)
                    }
                    this.$Progress.start()
                    next()
                })
                this.$router.afterEach((to, from) => {
                    this.$Progress.finish()
                })
            };
            Page['mounted'] = function () {
                this.$Progress.finish()
                this.$nextTick(() => {
                    if(typeof this.init == 'function') {
                        this.init();
                    }
                })
            };
        })
    }

    /**
     * @returns {Array}
     */
    getRoutes() {
        return this.routes;
    }
}