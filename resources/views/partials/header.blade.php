<header id="m_header" class="m-grid__item		m-header "  m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
    <div class="m-header__top">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- begin::Brand -->
                <div class="m-stack__item m-brand">
                    <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <router-link tag="a" :to="{name: 'tests'}" class="m-brand__logo-wrapper">
                                <img alt="" src="/img/logos/logo-2.png"/>
                            </router-link>
                        </div>
                    </div>
                </div>
                <!-- end::Brand -->
                <!-- begin::Topbar -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__welcome">
                                                Привет,&nbsp;
                                            </span>
                                        <span class="m-topbar__username">
                                                {{ auth()->user()->first_name }}
                                            </span>
                                        <span class="m-topbar__userpic">
                                                <img src="{{ auth()->user()->avatar_url ?? asset('img/no_avatar.png') }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                            </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url(img/misc/user_profile_bg.jpg); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="{{ auth()->user()->avatar_url ?? asset('img/no_avatar.png') }}" class="m--img-rounded m--marginless" alt=""/>
                                                    </div>
                                                    <div class="m-card-user__details">
                                                            <span class="m-card-user__name m--font-weight-500">
                                                                {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                                                            </span>
                                                        <span class="m-card-user__email m--font-weight-300 m-link">
                                                                {{ auth()->user()->email ?? 'VK: id' . auth()->user()->vk_id }}
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <router-link :to="{name: 'profile'}" class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
                                                                    <span class="m-nav__link-wrap">
                                                                        <span class="m-nav__link-text">
                                                                            Мой профиль
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </router-link>
                                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ route('logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                Выйти
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end::Topbar -->
            </div>
        </div>
    </div>
    <div class="m-header__bottom">
        <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- begin::Horizontal Menu -->
                <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
                        <i class="la la-close"></i>
                    </button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light "  >
                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                            <router-link tag="li" :to="{name: 'tests'}" class="m-menu__item "  aria-haspopup="true">
                                <a  href="#" class="m-menu__link ">
                                    <span class="m-menu__item-here"></span>
                                    <span class="m-menu__link-text">
                                        Тесты
                                    </span>
                                </a>
                            </router-link>
                            <router-link tag="li" :to="{name: 'profile'}" class="m-menu__item "  aria-haspopup="true">
                                <a  href="#" class="m-menu__link ">
                                    <span class="m-menu__item-here"></span>
                                    <span class="m-menu__link-text">
                                        Мой профиль
                                    </span>
                                </a>
                            </router-link>
                            
                            @if(auth()->user()->is_admin)
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" :class="{'m-menu__item--active': $route.name.indexOf('admin') !== -1}"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                                    <span class="m-menu__item-here"></span>
                                    <span class="m-menu__link-text">
                                        Администрирование
                                    </span>
                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:600px">
                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                    <div class="m-menu__subnav">
                                        <ul class="m-menu__content">
                                            <li class="m-menu__item">
                                                <h3 class="m-menu__heading m-menu__toggle">
                                                    <span class="m-menu__link-text">
                                                        Пользователи
                                                    </span>
                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                </h3>
                                                <ul class="m-menu__inner">
                                                    <router-link tag="li" :to="{name: 'admin.users'}" class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a  href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-list-1"></i>
                                                            <span class="m-menu__link-text">
                                                                Список пользователей
                                                            </span>
                                                        </a>
                                                    </router-link>
                                                    <router-link tag="li" :to="{name: 'admin.users.groups'}" class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a  href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-users"></i>
                                                            <span class="m-menu__link-text">
                                                                Список групп
                                                            </span>
                                                        </a>
                                                    </router-link>
                                                </ul>
                                            </li>
                                            <li class="m-menu__item">
                                                <h3 class="m-menu__heading m-menu__toggle">
                                                    <span class="m-menu__link-text">
                                                        Тесты
                                                    </span>
                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                </h3>
                                                <ul class="m-menu__inner">
                                                    <router-link tag="li" :to="{name: 'admin.tests'}" class="m-menu__item"  m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a  href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-list"></i>
                                                            <span class="m-menu__link-text">
                                                                Список тестов
                                                            </span>
                                                        </a>
                                                    </router-link>
                                                    <router-link tag="li" :to="{name: 'admin.tests.create'}" class="m-menu__item "  m-menu-link-redirect="1" aria-haspopup="true">
                                                        <a  href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-plus"></i>
                                                            <span class="m-menu__link-text">
                                                                Создать тест
                                                            </span>
                                                        </a>
                                                    </router-link>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <!-- end::Horizontal Menu -->
            </div>
        </div>
    </div>
</header>
