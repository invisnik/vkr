<!DOCTYPE html>
<html lang="ru" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        {{ config()->get('app.name') }}
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Open Sans:300,400,500,600,700:cyrillic","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ asset('css/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="../../../assets/demo/default/media/img/logo/favicon.ico" />
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div id="authApp" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url({{ asset('img/bg/bg-3.jpg') }});">
        <div class="m-login__wrapper-1 m-portlet-full-height">
            <div class="m-login__wrapper-1-1">
                <div class="m-login__contanier">
                    <div class="m-login__content">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ asset('img/logos/logo-2.png') }}">
                            </a>
                        </div>
                        <div class="m-login__title" style="padding-top: 3rem">
                            <h3>
                                Онлайн система психологической диагностики специалистов
                            </h3>
                        </div>
                        <div class="m-login__desc">
                            Если у вас нет аккаунта, вы можете создать его:
                        </div>
                        <div class="m-login__form-action" style="margin: 1rem 0 1rem 0">
                            <button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">
                                Создать аккаунт
                            </button>
                        </div>
                        <div class="m-login__desc" style="padding: 0">
                            Или войти через:
                        </div>
                        <div class="m-login__form-action" style="margin: 1rem 0 1rem 0">
                            <a href="{{ route('auth.vk') }}" class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air">
                                <i class="fa fa-vk"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="m-login__border">
                    <div></div>
                </div>
            </div>
        </div>
        <div class="m-login__wrapper-2 m-portlet-full-height">
            <div class="m-login__contanier">
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            Войти
                        </h3>
                    </div>
                    <form class="m-login__form m-form" method="post" action="{{ route('auth.login') }}">
                        <div class="form-group m-form__group" :class="{'has-danger': errors.loginErrors}">
                            <input class="form-control m-input" id="login_email" type="text" placeholder="Почта" name="email" v-model="loginData.email" autocomplete="off">
                            <div v-if="errors.loginErrors.email" class="form-control-feedback">@{{ errors.loginErrors.email[0] }}</div>
                        </div>
                        <div class="form-group m-form__group" :class="{'has-danger': errors.loginErrors}">
                            <input class="form-control m-input m-login__form-input--last" id="login_password" type="password" v-model="loginData.password" placeholder="Пароль" name="password">
                            <div v-if="errors.loginErrors.password" class="form-control-feedback">@{{ errors.loginErrors.password[0] }}</div>
                        </div>
                        <div class="row m-login__form-sub">
                            <div class="col m--align-left">
                                <label class="m-checkbox m-checkbox--focus">
                                    <input type="checkbox" name="remember" v-model="loginData.remember">
                                    Запомнить меня
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button @click.prevent="login" id="login_button" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Войти
                            </button>
                        </div>
                    </form>
                </div>
                <div class="m-login__signup">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            Создать аккаунт
                        </h3>
                        <div class="m-login__desc">
                            Введите ваши данные для создания аккаунта:
                        </div>
                    </div>
                    <form class="m-login__form m-form" method="post" action="{{ route('auth.register') }}">
                        <div class="form-group m-form__group" :class="{'has-danger': errors.registerErrors}">
                            <input class="form-control m-input" id="register_fullname" type="text" placeholder="ФИО" v-model="registerData.fullname" name="fullname">
                            <div v-if="errors.registerErrors.fullname" class="form-control-feedback">@{{ errors.registerErrors.fullname[0] }}</div>
                        </div>
                        <div class="form-group m-form__group" :class="{'has-danger': errors.registerErrors}">
                            <input class="form-control m-input" id="register_email" type="text" placeholder="Почта" v-model="registerData.email" name="email" autocomplete="off">
                            <div v-if="errors.registerErrors.email" class="form-control-feedback">@{{ errors.registerErrors.email[0] }}</div>
                        </div>
                        <div class="form-group m-form__group" :class="{'has-danger': errors.registerErrors}">
                            <input class="form-control m-input" id="register_password" type="password" placeholder="Пароль" v-model="registerData.password" name="password">
                            <div v-if="errors.registerErrors.password" class="form-control-feedback">@{{ errors.registerErrors.password[0] }}</div>
                        </div>
                        <div class="form-group m-form__group" :class="{'has-danger': errors.registerErrors}">
                            <input class="form-control m-input m-login__form-input--last" id="register_cpassword" type="password" placeholder="Повторите Пароль" v-model="registerData.password_confirmation" name="password_confirmation">
                            <div v-if="errors.registerErrors.password_confirmation" class="form-control-feedback">@{{ errors.registerErrors.password_confirmation[0] }}</div>
                        </div>
                        <div class="m-login__form-action">
                            <button @click.prevent="register" id="register_button" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Зарегистрироваться
                            </button>
                            <button id="m_login_signup_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                Назад
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{ asset('js/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
<script src="{{ asset('js/vendor.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/auth.js') }}" type="text/javascript"></script>
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
