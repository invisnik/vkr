<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestResult extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['text', 'points_from', 'points_to', 'test_id'];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }
}
