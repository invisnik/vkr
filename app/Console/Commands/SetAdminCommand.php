<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class SetAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Устанавливает роль администратора указаному пользователю';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->ask('Введите id пользователя:');

        $user = User::find($id);

        if (is_null($user)) {
            $this->error('Пользователь с таким id не найден.');
            return;
        }

        $user->is_admin = true;
        $user->save();

        $this->info('Вы успешно установили роль админа пользователю ' . $user->full_name);
    }
}
