<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestAnswer extends Model
{
    const TYPE_TEXT = 'text';
    const TYPE_IMG = 'img';

    protected $table = 'test_question_answers';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['text', 'img_url', 'type', 'question_id', 'points'];

    public function question()
    {
        return $this->belongsTo(TestQuestion::class, 'question_id');
    }
}
