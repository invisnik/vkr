<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTestResult extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'test_id', 'started_at', 'ended_at', 'points'];

    protected $dates = [
        'started_at', 'ended_at'
    ];

    protected $appends = ['test_result', 'time'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->belongsToMany(TestAnswer::class, 'user_answers', 'result_id', 'answer_id');
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function getTestResultAttribute()
    {
        foreach ($this->test->results as $result) {
            if ($result->points_from <= $this->points && $this->points <= $result->points_to) {
                return $result->text;
            }   
        }

        return null;
    }

    public function getTimeAttribute()
    {
        if(!is_null($this->ended_at)) {
            return $this->ended_at->diffInMinutes($this->started_at);
        }
    }
}
