<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\UserTestResult;
use App\TestAnswer;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\UserGroup;

class ApiController extends Controller
{


    public function tests($id = null)
    {
        if (is_null($id)) {
            return QueryBuilder::for(Test::class)->allowedIncludes(['questions.answers', 'results'])->get();
        } else {
            return QueryBuilder::for(Test::class)->allowedIncludes(['questions.answers', 'results'])->find($id);
        }
    }

    public function createTest(Request $request)
    {
        $test = Test::create($request->only(['name', 'desc']));

        $questions = $request->get('questions');
        $createdQuestions = $test->questions()->createMany($questions);;

        $i = 0;
        foreach ($createdQuestions as $question) {
            $question->answers()->createMany($questions[$i]['answers']);
            $i++;
        }

        $test->results()->createMany($request->get('results'));


        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Тест создан!',
            'status' => 'success'
        ]);
    }
    
    public function editTest(Test $test, Request $request)
    {
        $test->update($request->only(['name', 'desc']));

        $results = collect($request->get('results'));
        $questions = collect($request->get('questions'));

        foreach ($results as $result) {
            if (isset($result['id'])) {
                TestResult::where('id', $result['id'])->update($result);
            } else {
                TestResult::create($result);
            }
        }

        $resultIdsForDelete = $request->get('delete')['results'];
        TestResult::destroy($resultIdsForDelete);

        $questionIdsForDelete = $request->get('delete')['questions'];
        TestQuestion::destroy($questionIdsForDelete);

        $answersIdsForDelete = $request->get('delete')['answers'];
        TestAnswer::destroy($answersIdsForDelete);

        foreach ($questions as $question) {
            if (isset($question['id'])) {
                $q = TestQuestion::where('id', $question['id'])->update(collect($question)->except('answers')->toArray());

                foreach ($question['answers'] as $answer) {
                    if (isset($answer['id'])) {
                        TestAnswer::where('id', $answer['id'])->update($answer);
                    } else {
                        TestAnswer::create($answer);
                    }
                }
            } else {
                $q = TestQuestion::create($question);
                if (isset($question['answers'])) {
                    $q->answers()->createMany($question['answers']);
                }
            }

        }

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Тест сохранен!',
            'status' => 'success'
        ]);
    }

    public function deleteTest(Test $test)
    {
        $test->delete();

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Тест удален!',
            'status' => 'success'
        ]);
    }

    /**
     * Возврат результатов теста
     *
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function results(Request $request)
    {
        $result = QueryBuilder::for(UserTestResult::class)
            ->allowedFilters(['user_id', 'test_id'])
            ->allowedIncludes(['answers', 'test.questions.answers', 'test.results'])
            ->orderBy('id', 'desc')->first();

        if (is_null($result)) {
            $result = UserTestResult::create([
                'test_id' => $request->get('filter')['test_id'],
                'user_id' => $request->get('filter')['user_id']
            ]);
            $result = UserTestResult::with(['test.questions.answers', 'test.results', 'answers'])->find($result->id);
        }

        return $result;
    }

    /**
     * Сохранение результатов теста
     *
     * @param UserTestResult $result
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function saveResult(UserTestResult $result, Request $request)
    {
        $result->answers()->sync($ids = collect($request->get('answers'))->pluck('id'));

        if (count($ids) == $result->test->questions->count()) {
            $result->ended_at = Carbon::now();
        }

        $points = TestAnswer::whereIn('id', $ids)->sum('points');

        $result->points += $points;
        $result->save();

        return $result;
    }

    /**
     * Получить пользователя по id 
     * или вывести всех пользователей
     *
     * @param integer $id
     * 
     * @return JsonResponse
     */
    public function users(int $id = null)
    {
        if (is_null($id)) {
            return QueryBuilder::for(User::class)->allowedIncludes(['results.test.results'])->get();
        } else {
            return QueryBuilder::for(User::class)->allowedIncludes(['results.test.results'])->find($id);
        }
    }

    /**
     * Сохранение пользователя
     *
     * @param User $user
     * @param Request $request
     * 
     * @return JsonReponse
     */
    public function saveUser(User $user, Request $request)
    {
        list($first_name, $last_name) = explode(' ', $request->get('full_name'));
        $user->update([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $request->get('email'),
            'group_id' => $request->get('group_id'),
        ]);

        if ($request->has('is_admin')) {
            $user->is_admin = $request->get('is_admin');
        }

        if ($request->has('new_password')) {
            if ($request->get('new_password') === $request->get('new_password_confirmation')) {
                if (!is_null($user->password) && !Hash::check($request->get('old_password'), $user->password)) {
                    return response()->json([
                        'title' => 'Ошибка!',
                        'msg' => 'Неправильный старый пароль!',
                        'status' => 'error'
                    ]);
                }

                $user->password = Hash::make($request->get('new_password'));
            } else {
                return response()->json([
                    'title' => 'Ошибка!',
                    'msg' => 'Пароли не совпадают!',
                    'status' => 'error'
                ]);
            }
        }

        $user->save();

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Данные сохранены!',
            'status' => 'success'
        ]);
    }

    /**
     * Удаление пользователя
     *
     * @param User $user
     * 
     * @return JsonResponse
     */
    public function deleteUser(User $user)
    {
        $user->delete();

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Пользователь удален!',
            'status' => 'success'
        ]);
    }

    /**
     * Возвращение списка групп пользователей
     *
     * @param int $id
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function userGroups(int $id = null, Request $request)
    {
        if(is_null($id)) {
            return UserGroup::withCount('users')->get();
        } else {
            return UserGroup::find($id);
        }
    }

    /**
     * Создание группы пользователей
     *
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function createUserGroup(Request $request)
    {
        $group = UserGroup::create($request->all());

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Группа ' . $group->name . ' создана!',
            'status' => 'success'
        ]);
    }

    /**
     * Редактирование группы пользователей
     *
     * @param UserGroup $group
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function saveUserGroup(UserGroup $group, Request $request)
    {
        $group->update($request->all());

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Группа ' . $group->name . ' обновлена!',
            'status' => 'success'
        ]);
    }

    /**
     * Удаление группы
     *
     * @param UserGroup $group
     * 
     * @return JsonResponse
     */
    public function deleteUserGroup(UserGroup $group)
    {
        $group->delete();

        return response()->json([
            'title' => 'Успех!',
            'msg' => 'Группа удалена!',
            'status' => 'success'
        ]);
    }

    public function userGroupStats(UserGroup $group, Test $test)
    {
        $users = $group->users;

        $results = UserTestResult::where('test_id', $test->id)
        ->whereIn('user_id', $users->pluck('id'))
        ->with('user')
        ->get();

        return $results;
    }
}
