<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function vkRedirect()
    {
        return Socialite::with('vkontakte')->redirect();
    }

    public function vkHandle()
    {
        $vk_user = Socialite::with('vkontakte')->user();

        $user = User::where('vk_id', $vk_user->id)->first();
        if(is_null($user)) {
            $user = User::create([
                'first_name' => $vk_user->user['first_name'],
                'last_name' => $vk_user->user['last_name'],
                'email' => $vk_user->accessTokenResponseBody['email'],
                'vk_id' => $vk_user->id,
                'avatar_url' => $vk_user->avatar,
            ]);
        }

        auth()->login($user, true);

        return redirect()->route('index');
    }
}
