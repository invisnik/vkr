<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * @var bool
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;

    public function users()
    {
        return $this->hasMany(User::class, 'group_id');
    }
}
