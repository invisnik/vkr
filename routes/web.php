<?php

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('auth/login', 'Auth\LoginController@login')->name('auth.login');
Route::post('auth/register', 'Auth\RegisterController@register')->name('auth.register');
Route::get('auth/vk/redirect', 'Auth\SocialController@vkRedirect')->name('auth.vk');
Route::get('auth/vk/handle', 'Auth\SocialController@vkHandle')->name('auth.vk.handle');

Route::group(['middleware' => 'auth'], function() {
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    
    // Show index page
    Route::get('/{path?}', 'IndexController@index')->name('index')->where('path', '(.*)');;
});
