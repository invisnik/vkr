<?php

use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Test;
use App\TestResult;
use App\TestQuestion;
use App\TestAnswer;
use App\UserTestResult;
use App\User;
use App\UserGroup;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::bind('test', function ($id) {
    return Test::find($id);
});

Route::bind('user_result', function ($id) {
    return UserTestResult::find($id);
});

Route::bind('user', function ($id) {
    return User::find($id);
});

Route::bind('group', function ($id) {
    return UserGroup::find($id);
});

Route::get('/api', function(){
    return 'api';
})->name('api');

# Get test
Route::get('/tests/{id?}', ['uses' => 'ApiController@tests']);

# Create test
Route::post('/tests', ['uses' => 'ApiController@createTest']);

# Edit test
Route::put('/tests/{test}', ['uses' => 'ApiController@editTest']);

# Delete test
Route::delete('/tests/{test}', ['uses' => 'ApiController@deleteTest']);

# Get result
Route::get('/results', ['uses' => 'ApiController@results']);

# Save results
Route::put('/results/{user_result}', ['uses' => 'ApiController@saveResult']);

# Get user
Route::get('/users/{id?}', ['uses' => 'ApiController@users']);

# Save user
Route::put('/users/{user}', ['uses' => 'ApiController@saveUser']);

# Delete user
Route::delete('/users/{user}', ['uses' => 'ApiController@deleteUser']);

# Get groups
Route::get('/groups/{id?}', ['uses' => 'ApiController@userGroups']);

# Create group
Route::post('/groups', ['uses' => 'ApiController@createUserGroup']);

# Save user
Route::put('/groups/{group}', ['uses' => 'ApiController@saveUserGroup']);

# Delete user
Route::delete('/groups/{group}', ['uses' => 'ApiController@deleteUserGroup']);

# Group stats
Route::get('/groups/{group}/stats/{test}', ['uses' => 'ApiController@userGroupStats']);